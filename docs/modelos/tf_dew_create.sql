-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2021-02-06 00:24:33.567

-- tables
-- Table: carrito
CREATE TABLE carrito (
    id_carrito bigint  NOT NULL,
    codigo_pedido varchar(64)  NULL,
    token varchar(128)  NULL,
    total decimal(10,2)  NULL,
    fecha_venta timestamp  NULL DEFAULT CURRENT_TIMESTAMP,
    estado tinyint  NOT NULL DEFAULT 1,
    costo_envio decimal(10,2)  NULL,
    codigo_descuento varchar(45)  NULL,
    porcentaje_descuento varchar(45)  NULL,
    id_usuario mediumint(5)  NOT NULL,
    CONSTRAINT carritos_pk PRIMARY KEY  (id_carrito)
);

-- Table: carro_detalle
CREATE TABLE carro_detalle (
    id_carro_detalle int  NOT NULL,
    imagen varchar(128)  NULL,
    nombre varchar(256)  NULL,
    codigo varchar(64)  NULL,
    cantidad int(16)  NULL,
    precio decimal(10,2)  NULL,
    subtotal decimal(10,2)  NULL,
    peso varchar(128)  NULL,
    talla varchar(128)  NULL,
    estado tinyint  NOT NULL DEFAULT 1,
    id_producto bigint  NOT NULL,
    id_carrito bigint  NOT NULL,
    CONSTRAINT carro_detalle_pk PRIMARY KEY  (id_carro_detalle)
);

-- Table: categoria
CREATE TABLE categoria (
    id_categoria int  NOT NULL,
    nombre varchar(255)  COLLATE 'utf8_spanish_ci' NOT NULL,
    imagen varchar(255)  COLLATE 'utf8_spanish_ci' NOT NULL,
    orden int  NOT NULL,
    estado tinyint(1)  NOT NULL DEFAULT 1,
    CONSTRAINT categoria_pk PRIMARY KEY  (id_categoria)
);

-- Table: persona
CREATE TABLE persona (
    percod bigint  NOT NULL,
    apemat varchar(32)  NULL DEFAULT NULL,
    apepat varchar(32)  NULL DEFAULT NULL,
    nombres varchar(32)  NULL DEFAULT NULL,
    sexo varchar(1)  NULL DEFAULT NULL,
    email varchar(64)  NULL DEFAULT NULL,
    doctip tinyint(2)  NULL DEFAULT 1,
    docnro varchar(11)  NULL DEFAULT NULL,
    fecnac date  NULL DEFAULT NULL,
    edad tinyint(3)  NULL DEFAULT NULL,
    img varchar(32)  NULL DEFAULT NULL,
    created timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated timestamp  NULL,
    CONSTRAINT persona_pk PRIMARY KEY  (percod)
);

CREATE INDEX persona_doctip on persona (doctip ASC)
;

-- Table: producto
CREATE TABLE producto (
    id_producto bigint  NOT NULL,
    codigo varchar(16)  NOT NULL,
    orden bigint  NOT NULL DEFAULT 0,
    estado tinyint(1)  NOT NULL DEFAULT 0,
    url varchar(255)  NOT NULL DEFAULT '',
    nombre varchar(255)  NOT NULL DEFAULT '',
    resumen mediumtext  NOT NULL,
    descripcion mediumtext  NOT NULL,
    imagen varchar(255)  NOT NULL,
    destacado tinyint(1)  NOT NULL DEFAULT 0,
    oferta tinyint(1)  NULL,
    precio float(6,2)  NOT NULL,
    porcentaje int(2)  NOT NULL DEFAULT 0,
    precio_descuento float  NULL,
    id_categoria int  NOT NULL,
    CONSTRAINT producto_pk PRIMARY KEY  (id_producto)
);

-- Table: producto_galeria
CREATE TABLE producto_galeria (
    id_prod_galeria bigint  NOT NULL,
    imagen varchar(128)  NULL,
    imagen_title varchar(150)  NULL,
    estado tinyint(1)  NOT NULL,
    id_producto bigint  NOT NULL,
    CONSTRAINT producto_galeria_pk PRIMARY KEY  (id_prod_galeria)
);

-- Table: producto_tag
CREATE TABLE producto_tag (
    id_tag bigint  NOT NULL,
    id_producto bigint  NOT NULL,
    CONSTRAINT producto_tag_pk PRIMARY KEY  (id_producto,id_tag)
);

CREATE INDEX fk_producto_tag_tag1_idx on producto_tag (id_tag ASC)
;

-- Table: suscritos
CREATE TABLE suscritos (
    id int(10)  NOT NULL,
    email varchar(255)  NOT NULL,
    fecha datetime  NOT NULL,
    CONSTRAINT suscritos_pk PRIMARY KEY  (id)
);

-- Table: tag
CREATE TABLE tag (
    id_tag bigint  NOT NULL,
    tag varchar(16)  NOT NULL,
    descripcion varchar(255)  NULL,
    CONSTRAINT tag_pk PRIMARY KEY  (id_tag)
);

-- Table: usuarios
CREATE TABLE usuarios (
    id_usuario mediumint(5)  NOT NULL,
    usuario varchar(12)  NULL DEFAULT NULL,
    password varchar(12)  NULL DEFAULT NULL,
    nombre varchar(250)  NULL DEFAULT NULL,
    nivel tinyint(1)  NOT NULL DEFAULT '0',
    estado int(1)  NOT NULL DEFAULT '1',
    percod bigint  NOT NULL,
    CONSTRAINT usuarios_pk PRIMARY KEY  (id_usuario)
);

-- foreign keys
-- Reference: carrito_usuarios (table: carrito)
ALTER TABLE carrito ADD CONSTRAINT carrito_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios (id_usuario);

-- Reference: carro_detalle_carrito (table: carro_detalle)
ALTER TABLE carro_detalle ADD CONSTRAINT carro_detalle_carrito
    FOREIGN KEY (id_carrito)
    REFERENCES carrito (id_carrito);

-- Reference: carro_detalle_producto (table: carro_detalle)
ALTER TABLE carro_detalle ADD CONSTRAINT carro_detalle_producto
    FOREIGN KEY (id_producto)
    REFERENCES producto (id_producto);

-- Reference: fk_producto_tag_producto1 (table: producto_tag)
ALTER TABLE producto_tag ADD CONSTRAINT fk_producto_tag_producto1
    FOREIGN KEY (id_producto)
    REFERENCES producto (id_producto);

-- Reference: fk_producto_tag_tag1 (table: producto_tag)
ALTER TABLE producto_tag ADD CONSTRAINT fk_producto_tag_tag1
    FOREIGN KEY (id_tag)
    REFERENCES tag (id_tag);

-- Reference: producto_categoria (table: producto)
ALTER TABLE producto ADD CONSTRAINT producto_categoria
    FOREIGN KEY (id_categoria)
    REFERENCES categoria (id_categoria);

-- Reference: producto_galeria_producto (table: producto_galeria)
ALTER TABLE producto_galeria ADD CONSTRAINT producto_galeria_producto
    FOREIGN KEY (id_producto)
    REFERENCES producto (id_producto);

-- Reference: usuarios_persona (table: usuarios)
ALTER TABLE usuarios ADD CONSTRAINT usuarios_persona
    FOREIGN KEY (percod)
    REFERENCES persona (percod);

-- End of file.

