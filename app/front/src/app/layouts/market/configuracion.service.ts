import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable({
    providedIn: 'root'
}) 
export class ConfiguracionService {
    constructor(
        private http : HttpClient
    ) {

    }
    getConfig() {
        return this.http.get('https://localhost:44309/api/User/GetListUser')
        .toPromise();
    }
}
