import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormBuilder } from '@angular/forms';
import { SuscritosService} from 'src/app/services/suscritos.service';


@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit {
 

  constructor(private readonly suscritosservice: SuscritosService) { }
 


  

  getSuscritos(){
    this.suscritosservice.getSuscritos().subscribe((rest: any) =>{
      console.log(rest);
    })
  }

  ngOnInit(): void {
    this.getSuscritos();
  }

}