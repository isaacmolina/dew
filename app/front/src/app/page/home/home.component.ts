import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/models/categoria';
import {Producto} from '../../models/producto';
//const dataproductoOfertas : Array<Producto> = []
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public productoOfertas : Array<Producto>
  public topCategorias : Array<Categoria>
  public productosDestacados : Array<any>;
  public currentProducto: Producto;
  constructor() {
    this.productoOfertas = getProductos(10)
    this.currentProducto =  this.productoOfertas[0]
    this.topCategorias = getCategorias(8)
    this.productosDestacados = getProductos(12)
    console.log(this.productoOfertas)
    resizeCarousel(window)
   }

   ngOnInit(): void {
  }
  
}

const imgProductos: Array<string> = ['assets/img/product/product1.jpg','assets/img/product/product2.jpg','assets/img/product/product3.jpg','assets/img/product/product4.jpg','assets/img/product/product5.jpg','assets/img/product/product6.jpg','assets/img/product/product7.jpg','assets/img/product/product8.jpg','assets/img/product/product9.jpg','assets/img/product/product10.jpg','assets/img/product/product11.jpg','assets/img/product/product12.jpg','assets/img/product/product13.jpg','assets/img/product/product14.jpg','assets/img/product/product15.jpg','assets/img/product/product16.jpg','assets/img/product/product17.jpg','assets/img/product/product18.jpg','assets/img/product/product19.jpg','assets/img/product/product20.jpg','assets/img/product/product21.jpg','assets/img/product/product22.jpg','assets/img/product/product23.jpg','assets/img/product/product24.jpg','assets/img/product/product25.jpg','assets/img/product/product26.jpg','assets/img/product/product27.jpg'];
const imgCategorias: Array<string> = ['assets/img/custom-p/product1.jpg', 'assets/img/custom-p/product2.jpg', 'assets/img/custom-p/product3.jpg', 'assets/img/custom-p/product4.jpg','assets/img/custom-p/product6.jpg','assets/img/custom-p/product7.jpg'];
const getProductos = function (cantidad: number) {
  const data: Array<Producto> = [];
  for (let i: number = 1; i <= cantidad; i++) {
    let nombre: string = `Nombre del Producto 00${i}`
    let descripcion: string = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam, reiciendis maiores quidem aperiam, rerum vel recusandae'
     const thumbs : Array<string> = [imgProductos[i], imgProductos[i+1], imgProductos[i+2], imgProductos[i+3]]
    const item: Producto = new Producto(i, nombre, descripcion, 1, 'Categoria 01', 50, true, 70, 50, 5, imgProductos[i],thumbs)
    data.push(item)
  }
  return data
}
const getCategorias = function (cantidad: number) {
  const data : Array<Categoria> = [];
  let ni : number = 1;
  for (let i : number = 1; i <= cantidad; i++) {
    if(ni === 3) {
      ni = 0
    }
    let nombre: string = `Nombre de la categoria 00${i}`
    let descripcion: string = ''
    const item: Categoria = new Categoria(i, nombre, descripcion, imgCategorias[ni],i,i)
    data.push(item)
    ni++
  }
  return data
}
const getProductosDestacados = function () {
  const data = [
    { categoria: 'Categoría 1', productos: getProductos(6) },
    { categoria: 'Categoría 2', productos: getProductos(6) },
    { categoria: 'Categoría 3', productos: getProductos(6) }
  ]
  return data
}

function resizeCarousel(w:any) {
  return w.iniciar()
}
