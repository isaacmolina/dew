import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { routing, AppRoutingProviders } from './page.routing';
import { AppRoutingModule } from './page-routing.module';
import { HomeComponent } from './home/home.component';
import { DetalleProductoComponent } from './detalle-producto/detalle-producto.component';
import { ProductoComponent } from './components/producto/producto.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { ProductoDestacadoComponent } from './components/producto-destacado/producto-destacado.component';
import { ModalVistaRapidaComponent } from './components/modal-vista-rapida/modal-vista-rapida.component';
//import { LayoutComponent } from './page/layout/layout.component';

import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { ModificarUsuarioComponent } from './modificar-usuario/modificar-usuario.component';
import { AlertaEliminarComponent } from './alerta-eliminar/alerta-eliminar.component';
import { RegistrarseComponent } from './registrarse/registrarse.component';
import { TerminosComponent } from './terminos/terminos.component';
import { MicuentaComponent } from './micuenta/micuenta.component';
import { CarComponent } from './car/car.component';
//import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [
    HomeComponent,
    DetalleProductoComponent,
    ProductoComponent,
    CategoriaComponent,
    ProductoDestacadoComponent,
    ModalVistaRapidaComponent,
    //LayoutComponent
    IniciarSesionComponent,
    ListaUsuariosComponent,
    NuevoUsuarioComponent,
    ModificarUsuarioComponent,
    AlertaEliminarComponent,
    RegistrarseComponent,
    TerminosComponent,
    MicuentaComponent,
    CarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    //AdminModule,
    //routing
  ],
  providers: [{ provide: Window, useValue: window }],
  //bootstrap: [AppComponent]
})
export class PageModule { }
