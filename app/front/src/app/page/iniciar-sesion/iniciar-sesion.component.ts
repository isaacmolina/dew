import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.component.html',
  styleUrls: ['./iniciar-sesion.component.css']
})
export class IniciarSesionComponent implements OnInit {

  iniciarSesionForm = this.fb.group({
    person:this.fb.group({
      email:['', Validators.required],
      password: ['', Validators.required],
    }),
   
  });



  constructor(private fb: FormBuilder) {
   }

   cleanValues(){
    this.iniciarSesionForm.reset();
  }

 onSubmit(){
  console.log(this.iniciarSesionForm.value)
}


  ngOnInit(): void {
  }

}

