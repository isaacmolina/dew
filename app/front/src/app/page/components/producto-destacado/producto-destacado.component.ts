import { Component, Input, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-producto-destacado',
  templateUrl: './producto-destacado.component.html',
  styleUrls: ['./producto-destacado.component.css']
})
export class ProductoDestacadoComponent implements OnInit {
  @Input() data! : Producto
  constructor() { }

  ngOnInit(): void {
  }

}
