import { Component, OnInit, Input } from '@angular/core';
import { Categoria } from 'src/app/models/categoria';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  @Input() data! : Categoria;
  constructor() { }
  ngOnInit(): void {
  }

}
