import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVistaRapidaComponent } from './modal-vista-rapida.component';

describe('ModalVistaRapidaComponent', () => {
  let component: ModalVistaRapidaComponent;
  let fixture: ComponentFixture<ModalVistaRapidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalVistaRapidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVistaRapidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
