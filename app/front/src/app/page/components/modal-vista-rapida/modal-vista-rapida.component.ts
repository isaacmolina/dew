import { Component, Input, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-modal-vista-rapida',
  templateUrl: './modal-vista-rapida.component.html',
  styleUrls: ['./modal-vista-rapida.component.css']
})
export class ModalVistaRapidaComponent implements OnInit {
  @Input() data!: Producto;
  constructor() { }

  ngOnInit(): void {
    console.log(this.data)
  }

}
