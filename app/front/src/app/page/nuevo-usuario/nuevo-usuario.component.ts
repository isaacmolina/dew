import { Component, OnInit} from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms'
import { UserService } from 'src/app/page/nuevo-usuario/nuevo-usuario.service'

@Component({
  selector: 'app-registrarse',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.css']
})
export class NuevoUsuarioComponent implements OnInit {

  userForm = this.fb.group({
    Id_Usuario: ['',Validators.required],
    Nombres: [''],
    ApePat: [''],
    ApeMat: [''],
    Email: [''],
    Doctip: [''],
    Docnro: [''],
    Usuario: [''],
    Password: [''],
    Sexo: [''],
    Fecnac: [''],
    Edad: [''],
    Estado: [''],
    Nivel: ['']
    

  });

  constructor(private fb : FormBuilder, private readonly userService : UserService) { }
  
 userInsert(data: any){
    console.log('data:', data)
    this.userService.setUserInsert(data).subscribe((res) => {
      console.log(res) ;
    })

  }
   
onSubmit(){
  if(this.userForm.valid)
  {
    console.log('onSubmit')
    this.userInsert(this.userForm.value);
    window.location.href = "lista-usuarios"

    }
  }

  ngOnInit(): void {
  }

}
