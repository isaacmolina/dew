
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable ({
    providedIn: 'root'
})
export class UserServiceDelete {

    constructor(private readonly http: HttpClient) { }
    
    setUserDelete(body: any){
        console.log('body:', body)
        return this.http.post('https://localhost:44309/api/User/DeleteUsuario',body);

    }
}