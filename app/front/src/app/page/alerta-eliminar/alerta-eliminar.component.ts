import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { UserServiceDelete } from 'src/app/page/alerta-eliminar/alerta-eliminar.service';

@Component({
  selector: 'app-alerta-eliminar',
  templateUrl: './alerta-eliminar.component.html',
  styleUrls: ['./alerta-eliminar.component.css']
})
export class AlertaEliminarComponent implements OnInit {

  
  userFormDelete = this.fb.group({
    id_usuario: ['',Validators.required]
  });

    constructor(private fb : FormBuilder, private readonly userServiceDelete : UserServiceDelete) { }
  
    userDelete(data: any){
       console.log('data:', data)
       this.userServiceDelete.setUserDelete(data).subscribe((res) => {
         console.log(res) ;
       })
   
     }
      
   onSubmit(){
     if(this.userFormDelete.valid)
     {
       console.log('onSubmit')
       this.userDelete(this.userFormDelete.value);
       window.location.href = "iniciar-sesion"
   
       }
     }
   
     ngOnInit(): void {
     }
   
   }
     