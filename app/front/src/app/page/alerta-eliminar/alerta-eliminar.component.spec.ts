import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertaEliminarComponent } from './alerta-eliminar.component';

describe('AlertaEliminarComponent', () => {
  let component: AlertaEliminarComponent;
  let fixture: ComponentFixture<AlertaEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertaEliminarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertaEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
