import { Component, OnInit} from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms'
import { UserService } from 'src/app/page/registrarse/registrarse.service'

@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
  styleUrls: ['./registrarse.component.css']
})
export class RegistrarseComponent implements OnInit {

  userForm = this.fb.group({
    id_Usuario: [''],
    nombres: [''],
    apePat: [''],
    apeMat: [''],
    email: [''],
    doctip: [''],
    docnro: [''],
    usuario: [''],
    password: [''],
    sexo: [''],
    fecnac: [''],
    edad: [''],
    estado: [''],
    nivel: ['']
    

  });

  constructor(private fb : FormBuilder, private readonly userService : UserService) { }
  
 userInsert(data: any){
    console.log('data:', data)
    this.userService.setUserInsert(data).subscribe((res) => {
      console.log(res) ;
    })

  }
   
onSubmit(){
  if(this.userForm.valid)
  {
    console.log('onSubmit')
    this.userInsert(this.userForm.value);
    window.location.href = "terminos"

    }
  }

  ngOnInit(): void {
  }

}
