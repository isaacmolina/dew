import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactoComponent } from '../page/contacto/contacto.component';
import { ProductoComponent } from '../page/components/producto/producto.component';
import { HomeComponent } from '../page/home/home.component';
import { QuienesSomosComponent } from '../page/quienes-somos/quienes-somos.component';
import { DetalleProductoComponent } from '../page/detalle-producto/detalle-producto.component'
import { MarketComponent } from '../layouts/market/market.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { MicuentaComponent } from './micuenta/micuenta.component';
import { ModificarUsuarioComponent } from './modificar-usuario/modificar-usuario.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { RegistrarseComponent } from './registrarse/registrarse.component';
import { TerminosComponent } from './terminos/terminos.component';
import { AlertaEliminarComponent } from './alerta-eliminar/alerta-eliminar.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { CarComponent } from './car/car.component';

const routes: Routes = [
  {
    path:'',
    component: MarketComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'contacto', component: ContactoComponent },
      { path: 'producto', component: DetalleProductoComponent },
      { path: 'pedido', component: CarComponent },
      { path: 'quienessomos', component: QuienesSomosComponent },
      //{ path: 'detalle-producto', component: DetalleProductoComponent},
      {path: 'iniciar-sesion', component: IniciarSesionComponent},
      {path: 'micuenta', component: MicuentaComponent},
      {path: 'modificar-usuario', component: ModificarUsuarioComponent},
      {path: 'nuevo-usuario', component: NuevoUsuarioComponent},
      {path: 'lista-usuarios', component: ListaUsuariosComponent},
      {path: 'registrarse', component: RegistrarseComponent},
      {path: 'terminos', component:TerminosComponent},
      {path: 'alerta-eliminar', component: AlertaEliminarComponent}

      /*{ path: '', redirectTo: 'inicio', pathMatch: 'full' }*/
    ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
