import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
//rutas principales
import { AppRoutingModule } from './app-routing.module';
import { AdminComponent } from './layouts/admin/admin.component';
import { MarketComponent } from './layouts/market/market.component';
import { AdminModule } from './admin/admin.module';
import { PageModule } from './page/page.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
//import { AppRoutingProviders } from './page/page.routing';


@NgModule({
  declarations: [AppComponent, AdminComponent, MarketComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    PageModule,
    AdminModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
