import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/admin/services/producto.service';
import { Producto2 } from 'src/app/models/producto2';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public productos : Array<Producto2>;
  constructor(private readonly prodcutoService: ProductoService) {
    this.productos = []
  }
  getProductos(){
    this.prodcutoService.getProductos().then((res: any) => {

      console.log('prodcutoService.getProductos:', res);
      this.productos  = res && res.data ? res.data : res
    })
  }
  eliminar(item : Producto2) {
    console.log('item:', item)
    this.prodcutoService.eliminar(item.id_producto).then(res => {
      this.getProductos()  
    }).catch(e => {
      this.getProductos()  
    })
  }
  ngOnInit(): void {
    this.getProductos()
  }

}
