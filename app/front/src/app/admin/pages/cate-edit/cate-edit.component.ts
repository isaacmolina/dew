import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cate-edit',
  templateUrl: './cate-edit.component.html',
  styleUrls: ['./cate-edit.component.css']
})
export class CateEditComponent implements OnInit {
  name = new FormControl('');

  constructor(private fb: FormBuilder) { }
   cateForm = this.fb.group({
    categoria: this.fb.group({
      codigo:['', Validators.required],
      nombre: ['', Validators.required]
    }),
    estado:['']
  })


  
  updateValues() {
    this.cateForm.patchValue({
      categoria: {
        codigo: "C001",
        nombre: "Desayunos"
      },
      estado:"A"
    })
   
  }
  cleanValues() {
    this.cateForm.reset();
  }
  onSubmit(){
    console.log(this.cateForm.value)
  }
  ngOnInit(): void {
    this.updateValues();
  }
 
}
