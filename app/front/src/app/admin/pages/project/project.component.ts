
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contactus',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  
  name = new FormControl('');
  
 /* contactusForm = new FormGroup({

    person: new FormGroup({

      firstname: new FormControl(''),

      lastname: new FormControl('')

    }),

    

    email: new FormControl(''),

    address: new FormControl(''),

    gender: new FormControl('')

  });*/

  productForm1 = this.fb.group({
    product: this.fb.group({
      codigo:['', Validators.required],
      nombre: ['', Validators.required]
    }),
    precio:[''],
    stock:[''],
    foto:['']
  })

  constructor(private fb: FormBuilder) { }
  
  updateValues() {
    this.productForm1.patchValue({
      person: {
        codigo: "P001",
        nombre: "Salas"
      },
      precio:"14.8",
      stock:"12",
      foto:"F"
    })
   
  }
  cleanValues() {
    this.productForm1.reset();
  }
  onSubmit(){
    console.log(this.productForm1.value)
  }
  ngOnInit(): void {
    /*this.updateValues();*/
  }

}

/*import { Component, OnInit } from '@angular/core';

import { ProjectService } from 'src/app/services/project.service';

import { ActivatedRoute, Params } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';



@Component({

  selector: 'app-project',

  templateUrl: './project.component.html',

  styleUrls: ['./project.component.css']

})

export class ProjectComponent implements OnInit {



  projects = [];

  apartments = [];

  showProject = "true";


  constructor(private readonly projectService: ProjectService, private activateRoute: ActivatedRoute) { }



  getProjects(){

    this.projectService.getProjects().subscribe((rest: any) => {

      this.projects = rest.data;

      console.log(this.projects);

    })

  }



  getProjectById(id: number){

    this.projectService.getProjects().subscribe((rest: any) => {

      this.projects = rest.data.filter(item => item.id == id);

      console.log(this.projects);

    })

  }



  getApartmentsByProject(id: number){

    this.projectService.getApartments().subscribe((rest: any) => {

      this.apartments = rest.data.filter(item => item.projectId == id);

      console.log(this.apartments);

    })

  }



  ngOnInit(): void {

    this.activateRoute.params.subscribe((params: Params) => {

      if (params.id){

        this.getProjectById(params.id);

        this.getApartmentsByProject(params.id);
        this.showProject="false";

      }

      else {

        this.getProjects();

      }

    });

  }

}*/