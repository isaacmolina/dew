import { ArrayType } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/admin/services/lista-usuarios.service';
import { Usuario } from 'src/app/models/usuario';


@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  public usuarios : Array<Usuario>

  constructor(private readonly usuarioService: UserService) { 
    this.usuarios = []
  }

  getListUser(){
    this.usuarioService.getListUser().subscribe((res: any) => {
      this.usuarios  = res && res.data ? res.data : res
      console.log('this.usuarios:', this.usuarios);
  })
  
}
  
  ngOnInit(): void {
    this.getListUser();
  }
  
}