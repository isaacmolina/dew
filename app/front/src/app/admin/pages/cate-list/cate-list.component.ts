import { ArrayType, isNgTemplate } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { CateService } from 'src/app/admin/services/categoria.service';
import { Categoria } from 'src/app/models/categoria';

@Component({
  selector: 'app-cate-list',
  templateUrl: './cate-list.component.html',
  styleUrls: ['./cate-list.component.css']
})
export class CateListComponent implements OnInit {
  
  public categorias : Array<Categoria>

  constructor(private readonly categoriaService: CateService) { 
    this.categorias = []    
  }

  getCategorias(){
    this.categoriaService.getCategorias().subscribe((res: any) => {
      this.categorias  = res && res.data  ? res.data : res
      console.log('this.categorias:', this.categorias);
  })
  
}

  
  ngOnInit(): void {
    this.getCategorias();
  }
  
}
