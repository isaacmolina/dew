import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-product-featured',
  templateUrl: './product-featured.component.html',
  styleUrls: ['./product-featured.component.css']
})
export class ProductFeaturedComponent implements OnInit {
  public productos: Array<ProductosService> 
  

  constructor(private readonly ProductosService:ProductosService) { 
    this.productos=[];
  }
  getProductsFeatured(){
  
    this.ProductosService.getProductsFeatured().subscribe((rest:any)=>{
      console.log(rest);
      this.productos=rest;

    })
  }

  ngOnInit(): void {
    this.getProductsFeatured();
  }

}
