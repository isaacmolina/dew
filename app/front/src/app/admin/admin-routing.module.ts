import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../layouts/admin/admin.component';
//import { HomeComponent } from './home/home.component';
/*
import { TestComponent } from './test/test.component';
import { AboutusComponent } from './pages/aboutus/aboutus.component';
import { TeamComponent } from './pages/team/team.component';*/
//<<<<<<< HEAD
///<<<<<<< HEAD=======
//import { ApartmentComponent } from './pages/apartment/apartment.component';
//import { HomeComponent } from './pages/home/home.component';
//>>>>>>> 029405dc3f650cb064b88e7cbc7c4b1373250587
//=======

//>>>>>>> 4494af2cb4557e03b327961a1d661d045cae32d6
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductEditComponent } from './pages/product-edit/product-edit.component';
import { ProjectComponent } from './pages/project/project.component';
import { CateListComponent} from './pages/cate-list/cate-list.component';
import { ListaUsuariosComponent } from './pages/lista-usuarios/lista-usuarios.component';
import { AlertaEliminarComponent } from './pages/alerta-eliminar/alerta-eliminar.component';
import { CateEditComponent } from './pages/cate-edit/cate-edit.component';
import { ProductoDestacadoComponent } from '../page/components/producto-destacado/producto-destacado.component';
import { ProductFeaturedComponent } from './pages/product-featured/product-featured.component';
const routes: Routes = [
  {
    path:'',
    component: AdminComponent,
    children: [
      { path: 'admin/list', component: ProductListComponent },
      { path: 'admin/product-edit', component: ProductEditComponent },
      { path: 'admin/project', component: ProjectComponent },
      { path: 'admin/cate', component: CateListComponent },     
      { path: 'admin/cate-edit/:id', component: CateEditComponent },     
      { path: 'admin/list-usu', component: ListaUsuariosComponent },
      { path: 'admin/alerta', component: AlertaEliminarComponent },
      { path: 'product-featured',component:ProductFeaturedComponent}
    ]  
  }  
];  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
