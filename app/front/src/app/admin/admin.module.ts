import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AdminRoutingModule } from './admin-routing.module';
import { HomeComponent } from './home/home.component';
import { TestComponent } from './test/test.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { CateListComponent } from './pages/cate-list/cate-list.component';
import { ListaUsuariosComponent } from './pages/lista-usuarios/lista-usuarios.component';
import { AlertaEliminarComponent } from './pages/alerta-eliminar/alerta-eliminar.component';
import { ProjectComponent } from './pages/project/project.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ProductFeaturedComponent } from './pages/product-featured/product-featured.component';


@NgModule({
  declarations: [HomeComponent, TestComponent,
    /*ContactusComponent,
    AboutusComponent,
    TeamComponent,
    HomeComponent,
    ProjectComponent,
    ApartmentComponent,
    ProductEditComponent
    */
   ProductListComponent,
   ProjectComponent,
   CateListComponent,
   ListaUsuariosComponent,
   AlertaEliminarComponent,
   ProductFeaturedComponent
  
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AdminModule { }
