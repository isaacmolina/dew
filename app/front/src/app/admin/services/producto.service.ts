import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductoService {
  constructor(private readonly http: HttpClient) { }

  getProductos(){
    return this.http.get('https://localhost:44309/api/product/GetProducts').toPromise();
  }
  eliminar(id_producto: any){
    return this.http.get('https://localhost:44309/api/product/DeleteProducto?id_producto=' + id_producto).toPromise();
  }
  actualizar(){
    return this.http.get('https://localhost:44309/api/product/GetProducts').toPromise();
  }

}