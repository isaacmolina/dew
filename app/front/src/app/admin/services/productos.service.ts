import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductosService {
  constructor(private readonly http: HttpClient) { }

  getProductsFeatured(){

    return this.http.get('https://localhost:44309/api/product/FeaturedProducts');

  }

}
