import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private readonly http: HttpClient) { }

  getListUser(){

    return this.http.get('https://localhost:44309/api/User/GetListUser');

  }

}