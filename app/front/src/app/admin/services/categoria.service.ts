import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CateService {
  constructor(private readonly http: HttpClient) { }

  getCategorias(){

    return this.http.get('https://localhost:44309/api/Categoria/GetCategorias');

  }

  getCatebyid(id:number){

    return this.http.get('https://localhost:44309/api/Categoria/GetCatbyId').toPromise();
    
  }

}