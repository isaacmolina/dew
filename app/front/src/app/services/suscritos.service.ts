import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SuscritosService {

  constructor(private readonly http: HttpClient ) { }
  getSuscritos(){
    return this.http.get('https://localhost:44309/api/suscritos/GetSuscritos');
  }
}