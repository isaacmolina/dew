import { TestBed } from '@angular/core/testing';

import { SuscritosService } from './suscritos.service';

describe('SuscritosService', () => {
  let service: SuscritosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SuscritosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
