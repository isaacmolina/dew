export class Categoria {
    constructor(
        public id_Categoria : number,
        public nombre : string,
        public descripcion : string,
        public imagen : string,
        public orden : number,
        public estado : number
    ) {
    }
}