export class Producto2 {
    constructor(
        public codigo : string,
        public descripcion : string,
        public destacado: number,
        public estado: number,
        public id_categoría: number,
        public id_producto: number,
        public imagen : string,
        public nombre : string,
        public oferta: number,
        public orden: number,
        public porcentaje: string,
        public precio: number,
        public precio_descuento: number,
        public resumen : string,
        public url : string,
    ) {
    }
}