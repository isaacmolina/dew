export class Producto {
    constructor(
        public id : number,
        public nombre : string,
        public descripcion : string,
        public idCategoria : number,
        public nombreCategoria : string,
        public stock : number,
        public oferta : boolean,
        public precio : number,
        public precioOferta : number,
        public stars : number,
        public imagen : string,
        public thumbs : Array<any>
    ) {
    }
}