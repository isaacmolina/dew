export class Usuario {
    constructor(
        public id_Usuario : number,
        public usuario : string,
        public password : string,
        public apeMat : string,
        public apePat : string,
        public nombres : string,
        public sexo : string,
        public email : string,
        public nivel : number,
        public estado : number,
        public doctip : number,
        public docnro : number,
        public fecnac : number,
        public edad : number
    ) {
    }
}

