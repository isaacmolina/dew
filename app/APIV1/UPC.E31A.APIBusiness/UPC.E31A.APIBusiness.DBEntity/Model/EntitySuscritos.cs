﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntitySuscritos : EntityBase
    {
        public int id { get; set; }
        public string email { get; set; }
        public DateTime fecha { get; set; }

    }
}