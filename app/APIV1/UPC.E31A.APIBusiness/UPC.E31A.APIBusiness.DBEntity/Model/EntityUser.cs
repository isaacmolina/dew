﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityUser : EntityBase
    {
        public List<Options> options;

        public int Id_Usuario { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string ApeMat { get; set; }
        public string ApePat { get; set; }
        public string Nombres { get; set; }
        public string Sexo { get; set; }
        public string Email { get; set; }
        public string Nivel { get; set; }
        public int Estado { get; set; }
        public int Doctip { get; set; }
        public string Docnro { get; set; }
        public string Fecnac { get; set; }
        public int Edad { get; set; }

    
        //
    }
    public class Options
    {
        public int id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
    }
}
