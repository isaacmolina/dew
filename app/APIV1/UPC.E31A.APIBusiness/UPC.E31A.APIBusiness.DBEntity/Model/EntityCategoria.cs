﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityCategoria
    {
		public int IdCategoria { get; set; }
		public string Nombre { get; set; }
		public string Imagen { get; set; }
		public string Orden { get; set; }
		public string Estado { get; set; }
	}
}
