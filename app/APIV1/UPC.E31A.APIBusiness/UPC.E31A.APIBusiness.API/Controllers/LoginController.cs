﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.Business.API.Controllers

{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {

        /// <summary>
        /// Constructor
        /// </summary>
        protected readonly ILoginRepository _LoginRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LoginRepository"></param>
        public LoginController(ILoginRepository LoginRepository)
        {
            _LoginRepository = LoginRepository;
        }
        /// <summary>
        /// GetListLogin
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("Getlogin")]
        [AllowAnonymous]
        [HttpPost]
        [Route("Getlogin")]
        public ActionResult Getlogin(string login, string password)
        {
            var ret = _LoginRepository.Getlogin(login, password);
            if (ret == null)
                return StatusCode(401);
            return Json(ret);


        }
    }
}