﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.Business.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {

        /// <summary>
        /// Constructor
        /// </summary>
        protected readonly IUserRepository _UserRepository;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserRepository"></param>
        public UserController(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;

        }

        /// <summary>
        /// GetListUser
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetListUser")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetListUser")]
        public ActionResult GetListUser()

        {
            var ret = _UserRepository.GetUsers();

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }


        /// <summary>
        /// GetUserByNomUsuario
        /// </summary>
        /// <param name="Usuario"></param>

        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetUserByNomUsuario")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetUserByNomUsuario")]
        public ActionResult GetUserByNomUsuario(string Usuario)
        {

            var ret = _UserRepository.GetUserByNomUsuario(Usuario);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetLoginUsuario")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetLoginUsuario")]
        public ActionResult GetLoginUsuario(string login, string password)
        {
            var ret = _UserRepository.GetLoginUsuario(login, password);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }


        /// <summary>
        /// InsertUsuario
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]

        [AllowAnonymous]
        [HttpPost]
        [Route("InsertUsuario")]
        public void InsertUsuario(EntityUser User)
        {
            _UserRepository.InsertUsuario(User);
        }





        /// <summary>
        /// GetUserByIdUsuario
        /// </summary>
        /// <param name="IdNum"></param>

        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetUserByIdUsuario")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetUserByIdUsuario")]
        public ActionResult GetUserByIdUsuario(int IdNum)
        {

            var ret = _UserRepository.GetUserByIdUsuario(IdNum);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// DeleteUsuario
        /// </summary>
        /// <param name="id_usuario"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("DeleteUsuario")]
        [AllowAnonymous]
        [HttpGet]
        [Route("DeleteUsuario")]
        public ActionResult DeleteUsuario(int id_usuario)
        {

            var ret = _UserRepository.DeleteUsuario(id_usuario);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// UpdateUsuario
        /// </summary>
       /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("UpdateUsuario")]
        [AllowAnonymous]
        [HttpGet]
        [Route("UpdateUsuario")]
        public ActionResult UpdateUsuario(int id_usuario, string password)
        {

            var ret = _UserRepository.UpdateUsuario(id_usuario, password);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }


    }
}
