﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;



namespace UPC.Business.API.Controllers

{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/Categoria")]
    public class CategoriaController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly ICategoriaRepository _CategoriaRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoriaRepository"></param>
        public CategoriaController(ICategoriaRepository CategoriaRepository)
        {
            _CategoriaRepository = CategoriaRepository;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetCategorias")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetCategorias")]
        public ActionResult GetCategorias()
        {
            var ret = _CategoriaRepository.GetCategorias();
            if (ret == null)
                return StatusCode(401);
            return Json(ret);
        }

        /// <summary>
        /// InsertCategoria
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]

        [AllowAnonymous]
        [HttpPost]
        [Route("InsertCategoria")]
        public void InsertCategoria(EntityCategoria Cate)
        {
            _CategoriaRepository.InsertCategoria(Cate);
        }

        /// <summary>
        /// DeleteCategoria
        /// </summary>
        /// <param name="id_categoria"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("DeleteCategoria")]
        [AllowAnonymous]
        [HttpGet]
        [Route("DeleteCategoria")]
        public ActionResult DeleteCategoria(int id_categoria)
        {

            var ret = _CategoriaRepository.DeleteCategoria(id_categoria);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// UpdateCategoria
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("UpdateCategoria")]
        [AllowAnonymous]
        [HttpGet]
        [Route("UpdateCategoria")]
        public ActionResult UpdateCategoria(int id_categoria, string nombre, string imagen, int orden, int estado)
        {

            var ret = _CategoriaRepository.UpdateCategoria(id_categoria, nombre, imagen, orden, estado);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
    }
}
