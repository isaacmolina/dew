﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.Business.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/Configuracion")]
    public class ConfiguracionController : Controller
    {

        /// <summary>
        /// Constructor
        /// </summary>
        protected readonly IConfiguracionRepository _ConfiguracionRepository;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConfiguracionRepository"></param>
        public ConfiguracionController(IConfiguracionRepository ConfiguracionRepository)
        {
            _ConfiguracionRepository = ConfiguracionRepository;

        }

        /// <summary>
        /// GetListConfiguracion
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetListConfiguracion")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetListConfiguracion")]
        public ActionResult GetListConfiguracion()

        {
            var ret = _ConfiguracionRepository.GetConfiguraciones();

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }


        /// <summary>
        /// GetConfiguracionByLlave
        /// </summary>
        /// <param name="Configuracion"></param>

        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetConfiguracionByLlave")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetConfiguracionByLlave")]
        public ActionResult GetConfiguracionByLlave(string llave)
        {

            var ret = _ConfiguracionRepository.GetConfiguracionByLlave(llave);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }


/*

        /// <summary>
        /// InsertConfiguracion
        /// </summary>
        /// <returns></returns>
        [Produces("application/json")]

        [AllowAnonymous]
        [HttpPost]
        [Route("InsertConfiguracion")]
        public void InsertConfiguracion(EntityConfiguracion Configuracion)
        {
            _ConfiguracionRepository.InsertConfiguracion(Configuracion);
        }





        /// <summary>
        /// GetConfiguracionByIdConfiguracion
        /// </summary>
        /// <param name="IdNum"></param>

        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetConfiguracionByIdConfiguracion")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetConfiguracionByIdConfiguracion")]
        public ActionResult GetConfiguracionByIdConfiguracion(int IdNum)
        {

            var ret = _ConfiguracionRepository.GetConfiguracionByIdConfiguracion(IdNum);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// DeleteConfiguracion
        /// </summary>
        /// <param name="id_Configuracion"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("DeleteConfiguracion")]
        [AllowAnonymous]
        [HttpGet]
        [Route("DeleteConfiguracion")]
        public ActionResult DeleteConfiguracion(int id_Configuracion)
        {

            var ret = _ConfiguracionRepository.DeleteConfiguracion(id_Configuracion);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        /// <summary>
        /// UpdateConfiguracion
        /// </summary>
       /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("UpdateConfiguracion")]
        [AllowAnonymous]
        [HttpGet]
        [Route("UpdateConfiguracion")]
        public ActionResult UpdateConfiguracion(int id_Configuracion, string password)
        {

            var ret = _ConfiguracionRepository.UpdateConfiguracion(id_Configuracion, password);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
*/

    }
}
