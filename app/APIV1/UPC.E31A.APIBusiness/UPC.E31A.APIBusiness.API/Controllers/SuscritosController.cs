﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.Business.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/suscritos")]
    public class SuscritosController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly ISuscritosRepository _SuscritosRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SuscritosRepository"></param>

        public SuscritosController(ISuscritosRepository SuscritosRepository)
        {

            _SuscritosRepository = SuscritosRepository;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [SwaggerOperation("GetSuscritos")]
        [AllowAnonymous]
        [HttpGet]
        [Route("GetSuscritos")]
        public ActionResult GetSuscritos(string email)
        {
            var ret = _SuscritosRepository.GetSuscritos(email);
            if (ret == null)
                return StatusCode(401);
            return Json(ret);


        }

    }
}
