﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IUserRepository
    {
        List<EntityUser> GetUsers();
        EntityUser GetUserByNomUsuario(string Usuario);
        EntityUser GetUserByIdUsuario(int IdNum);
        EntityUser GetLoginUsuario(string login, string password);
        void InsertUsuario(EntityUser User);
        EntityUser DeleteUsuario(int id_usuario);
        EntityUser UpdateUsuario(int id_usuario, string password);

    }
}
