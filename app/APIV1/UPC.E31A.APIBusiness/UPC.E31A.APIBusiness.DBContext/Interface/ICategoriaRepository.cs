﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface ICategoriaRepository
    {
        //List<EntityCategoria> GetCategorias();
        ResponseBase GetCategorias();
        void InsertCategoria(EntityCategoria Cate);
        //EntityCategoria DeleteCategoria(int id_categoria);
        ResponseBase DeleteCategoria(int id_categoria);
        //EntityCategoria UpdateCategoria(int id_categoria, string nombre, string imagen, int orden, int estado);
        ResponseBase UpdateCategoria(int id_categoria, string nombre, string imagen, int orden, int estado);
    }
}
