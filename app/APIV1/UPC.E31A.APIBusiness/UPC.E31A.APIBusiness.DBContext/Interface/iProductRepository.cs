﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface iProductRepository
    {
        List<EntityProducto> GetProducts();
        List<EntityProducto> GetProductByParams(int id_producto, string nombre);
        void InsertProduct(EntityProducto Cate);
        EntityProducto DeleteProducto(int id_producto);
        EntityProducto UpdateProducto(int id_producto, string codigo, int orden, int estado, string url, string nombre, string resumen, string descripcion, string imagen, int destacado, int oferta, decimal precio, int porcentaje, decimal precio_descuento, int id_categoria);
        List<EntityProducto> FeaturedProducts();
    }

}

