﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class ProductRepository : BaseRepository, iProductRepository
    {
        public List<EntityProducto> GetProducts()
        {
            var returnEntity = new List<EntityProducto>();
            using (var db = GetSqlConnection())
            {
                const string sql = @"list_productos";

                returnEntity = db.Query<EntityProducto>(sql, commandType: CommandType.StoredProcedure).ToList();
            }

            return returnEntity;
        }
    
        public List<EntityProducto> GetProductByParams(int id_producto, string nombre)
        {
            var returnEntity = new List<EntityProducto>();

            try
            {

                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@productid", value: id_producto, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@nombre", value: nombre, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"buscar_productos";

                    returnEntity = db.Query<EntityProducto>(sql, p, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnEntity;
        }
        public void InsertProduct(EntityProducto Cate)
        {
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@productid", value: Cate.id_producto, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@codigo", value: Cate.orden, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@orden", value: Cate.orden, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@estado", value: Cate.estado, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@url", value: Cate.url, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nombre", value: Cate.nombre, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@resumen", value: Cate.resumen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@descripcion", value: Cate.descripcion, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@imagen", value: Cate.imagen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@destacado", value: Cate.destacado, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@oferta", value: Cate.oferta, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@precio", value: Cate.precio, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                    p.Add(name: "@porcentaje", value: Cate.porcentaje, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@precio_descuento", value: Cate.precio_descuento, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                    p.Add(name: "@id_categoria", value: Cate.id_categoría, dbType: DbType.Int64, direction: ParameterDirection.Input);

                    const string sql = @"InsertarProducto";

                    db.Query<EntityProducto>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public EntityProducto DeleteProducto(int id_producto)
        {
            var returnEntity = new EntityProducto();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@productid", value: id_producto, dbType: DbType.Int64, direction: ParameterDirection.Input);

                    const string sql = @"EliminarProductoPorID";

                    returnEntity = db.Query<EntityProducto>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }
        public EntityProducto UpdateProducto(int id_producto, string codigo, int orden, int estado, string url, string nombre, string resumen, string descripcion, string imagen, int destacado, int oferta, decimal precio, int porcentaje, decimal precio_descuento, int id_categoria)
        {
            var returnEntity = new EntityProducto();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@productid", value: id_producto, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@codigo", value: orden, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@orden", value: orden, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@estado", value: estado, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@url", value: url, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nombre", value: nombre, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@resumen", value: resumen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@descripcion", value: descripcion, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@imagen", value: imagen, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@destacado", value: destacado, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@oferta", value: oferta, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@precio", value: precio, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                    p.Add(name: "@porcentaje", value: porcentaje, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    p.Add(name: "@precio_descuento", value: precio_descuento, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                    p.Add(name: "@id_categoria", value: id_categoria, dbType: DbType.Int64, direction: ParameterDirection.Input);

                    const string sql = @"UpdateProductoporID";

                    returnEntity = db.Query<EntityProducto>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }
        public List<EntityProducto> FeaturedProducts()
        {
            var returnEntity = new List<EntityProducto>();
            using (var db = GetSqlConnection())
            {
                const string sql = @"sp_ProductosDestacados";

                returnEntity = db.Query<EntityProducto>(sql, commandType: CommandType.StoredProcedure).ToList();
            }

            return returnEntity;
        }



    }
}
