﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class SuscritosRepository : BaseRepository, ISuscritosRepository
    {

        public ResponseBase GetSuscritos(string email)
        {
            ResponseBase responseBase = new ResponseBase();
            var returnEntity = new EntitySuscritos();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@email", value: email, dbType: DbType.String, ParameterDirection.Input);


                    const string sql = @"usp_insertarsuscrito";
                    returnEntity = db.Query<EntitySuscritos>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                responseBase.isSuccess = true;
                responseBase.codeError = "000";
                responseBase.messageError = string.Empty;
                responseBase.message = "Suscripcion correcta";
                responseBase.data = returnEntity;


            }
            catch (Exception ex)
            {
                responseBase.isSuccess = false;
                responseBase.codeError = "999";
                responseBase.messageError = ex.Message;
                responseBase.message = String.Empty;
                responseBase.data = returnEntity;
            }
            return responseBase;
        }

    }
}