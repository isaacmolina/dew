﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public List<EntityUser> GetUsers()
        {
            var returnEntity = new List<EntityUser>();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_ObtenerUsuarios";


                    returnEntity = db.Query<EntityUser>(sql,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }

        public EntityUser GetUserByNomUsuario(string Usuario)
        {
            var returnEntity = new EntityUser();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@NOMBRE", value: Usuario, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"sp_BuscarUsuarioPorNom";

                    returnEntity = db.Query<EntityUser>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }

        public EntityUser GetUserByIdUsuario(int IdNum)
        {
            var returnEntity = new EntityUser();
            ///
            ///
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@NUM", value: IdNum, dbType: DbType.Int32, direction: ParameterDirection.Input);
                   

                    const string sql = @"sp_BuscarUsuarioPorId";

                    returnEntity = db.Query<EntityUser>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

            return returnEntity;
        }


       

        public EntityUser GetLoginUsuario(string login, string password)
        {
            var returnEntity = new EntityUser();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@USUARIO", value: login, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@PASSWORD", value: password, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"sp_BuscarUsuario_ValidarLogin";
                    returnEntity = db.Query<EntityUser>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (returnEntity != null)
                    {
                        var id = returnEntity.Id_Usuario;
                        returnEntity = GetUserByIdUsuario(id);

                        //Colocar codigo para obtener las opciones por perfil de usuario.
                        List<Options> options = new List<Options>();
                        options.Add(new Options() { id = 1, name = "Option 1", path = "/path1" });
                        options.Add(new Options() { id = 2, name = "Option 2", path = "/path2" });
                        options.Add(new Options() { id = 3, name = "Option 3", path = "/path3" });
                        options.Add(new Options() { id = 4, name = "Option 4", path = "/path4" });
                        options.Add(new Options() { id = 5, name = "Option 5", path = "/path5" });

                        returnEntity.options = options;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }

        public void InsertUsuario(EntityUser User)
        {
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@PERCOD", value: User.Id_Usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@USUARIO", value: User.Usuario, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@PASSWORD", value: User.Password, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@NOMBRES", value: User.Nombres, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@APELLIDOPATERNO", value: User.ApePat, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@APELLIDOMATERNO", value: User.ApeMat, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@SEXO", value: User.Sexo, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@EMAIL", value: User.Email, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@TIPODOCUMENTO", value: User.Doctip, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@NUMERODOCUMENTO", value: User.Docnro, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@FECHANAC", value: User.Fecnac, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@EDAD", value: User.Edad, dbType: DbType.Int32, direction: ParameterDirection.Input);
                  

                    const string sql = @"sp_InsertarUsuario";

                    db.Query<EntityUser>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public EntityUser DeleteUsuario(int id_usuario)
        {
            var returnEntity = new EntityUser();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@id_usuario", value: id_usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);

                    const string sql = @"sp_EliminarUsuarioPorID";

                    returnEntity = db.Query<EntityUser>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }

        public EntityUser UpdateUsuario(int id_usuario, string password)
        {
            var returnEntity = new EntityUser();
            try
            {
                using (var db = GetSqlConnection())
                {
                    var p = new DynamicParameters();
                    p.Add(name: "@id_usuario", value: id_usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@password", value: password, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"sp_UpdateUsuarioporID";

                    returnEntity = db.Query<EntityUser>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }
    }
    }
