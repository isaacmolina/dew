﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class ConfiguracionRepository : BaseRepository, IConfiguracionRepository
    {
        public List<EntityConfiguracion> GetConfiguraciones()
        {
            var returnEntity = new List<EntityConfiguracion>();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_ObtenerUsuarios";


                    returnEntity = db.Query<EntityConfiguracion>(sql,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnEntity;
        }

        object IConfiguracionRepository.GetConfiguracionByLlave(string llave)
        {
            throw new NotImplementedException();
        }
        /*
       public EntityConfiguracion GetConfiguracionByLlave(string llave)
       {
           var returnEntity = new EntityConfiguracion();
           try
           {
               using (var db = GetSqlConnection())
               {
                   var p = new DynamicParameters();
                   p.Add(name: "@NOMBRE", value: Usuario, dbType: DbType.String, direction: ParameterDirection.Input);

                   const string sql = @"sp_BuscarUsuarioPorNom";

                   returnEntity = db.Query<EntityConfiguracion>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }

           return returnEntity;
       }

       public EntityConfiguracion GetConfiguracionById(int IdNum)
       {
           var returnEntity = new EntityConfiguracion();
           ///
           ///
           try
           {
               using (var db = GetSqlConnection())
               {
                   var p = new DynamicParameters();
                   p.Add(name: "@NUM", value: IdNum, dbType: DbType.Int32, direction: ParameterDirection.Input);


                   const string sql = @"sp_BuscarUsuarioPorId";

                   returnEntity = db.Query<EntityConfiguracion>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);

           }

           return returnEntity;
       }
       public void InsertConfiguracion(EntityConfiguracion Configuracion)
       {
           try
           {
               using (var db = GetSqlConnection())
               {
                   var p = new DynamicParameters();
                   p.Add(name: "@PERCOD", value: Configuracion.Id_Usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);
                   p.Add(name: "@USUARIO", value: Configuracion.Usuario, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@PASSWORD", value: Configuracion.Password, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@NOMBRES", value: Configuracion.Nombres, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@APELLIDOPATERNO", value: Configuracion.ApePat, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@APELLIDOMATERNO", value: Configuracion.ApeMat, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@SEXO", value: Configuracion.Sexo, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@EMAIL", value: Configuracion.Email, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@TIPODOCUMENTO", value: Configuracion.Doctip, dbType: DbType.Int32, direction: ParameterDirection.Input);
                   p.Add(name: "@NUMERODOCUMENTO", value: Configuracion.Docnro, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@FECHANAC", value: Configuracion.Fecnac, dbType: DbType.String, direction: ParameterDirection.Input);
                   p.Add(name: "@EDAD", value: Configuracion.Edad, dbType: DbType.Int32, direction: ParameterDirection.Input);


                   const string sql = @"sp_InsertarUsuario";

                   db.Query<EntityConfiguracion>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public EntityConfiguracion DeleteConfiguracion(int id)
       {
           var returnEntity = new EntityConfiguracion();
           try
           {
               using (var db = GetSqlConnection())
               {
                   var p = new DynamicParameters();
                   p.Add(name: "@id_usuario", value: id_usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);

                   const string sql = @"sp_EliminarUsuarioPorID";

                   returnEntity = db.Query<EntityConfiguracion>(sql, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }

           return returnEntity;
       }*/
    }
    }
