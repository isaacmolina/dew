﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class LoginRepository : BaseRepository, ILoginRepository
    {
        public ResponseBase Getlogin(string login, string password)
        {
            ResponseBase responseBase = new ResponseBase();
            var returnEntity = new EntityLogin();

            try
            {
                using (var db = GetSqlConnection())
                {

                    var p = new DynamicParameters();
                    p.Add(name: "@USUARIO", value: login, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@PASSWORD", value: password, dbType: DbType.String, direction: ParameterDirection.Input);

                    const string sql = @"sp_BuscarUsuario_ValidarLogin";
                    returnEntity = db.Query<EntityLogin>(sql, param: p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                }
                responseBase.isSuccess = true;
                responseBase.codeError = "000";
                responseBase.messageError = string.Empty;
                responseBase.message = "Usuario Correcto";
                responseBase.data = returnEntity;
            }
            catch (Exception ex)
            {

                responseBase.isSuccess = false;
                responseBase.codeError = "999";
                responseBase.messageError = ex.Message;
                responseBase.message = String.Empty;
                responseBase.data = null;
            }
            return responseBase;

        }
    }
}